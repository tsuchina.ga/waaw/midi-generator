package main

import (
    "bufio"
    "log"
    "os"

    "github.com/algoGuy/EasyMIDI/smf"
    "github.com/algoGuy/EasyMIDI/smfio"
    "strconv"
    "encoding/hex"
    "fmt"
)

const ticks = 960
var bpm = 120

func main() {
    // Create division
    division, err := smf.NewDivision(ticks, smf.NOSMTPE)
    checkErr(err)

    // Create new midi struct
    midi, err := smf.NewSMF(smf.Format1, *division)
    checkErr(err)

    // Create track struct
    track := &smf.Track{}

    bpm = 240
    tempo := int64(60 * 1000000 / bpm) // 60s * 1,000,000us / bpm
    tempoHex := fmt.Sprintf("%06s", strconv.FormatInt(tempo, 16))
    tempoByte, _ := hex.DecodeString(tempoHex)

    addMetaEvent(track, 0, smf.MetaSetTempo, tempoByte)
    addMidiEvent(track, 0, smf.ProgramChangeStatus, 0x00, 0x00, 0x00)

    schedule := schedule{}
    schedule.add(1, 1, 4, 0x00, 0x30, 0x50)
    schedule.add(1, 5, 4, 0x00, 0x30, 0x50)
    schedule.add(1, 9, 4, 0x00, 0x30, 0x50)
    schedule.add(1, 13, 4, 0x00, 0x30, 0x50)

    schedule.add(2, 1, 4, 0x00, 0x30, 0x50)
    schedule.add(2, 5, 4, 0x00, 0x30, 0x50)
    schedule.add(2, 9, 4, 0x00, 0x30, 0x50)
    schedule.add(2, 13, 4, 0x00, 0x30, 0x50)

    schedule.add(3, 1, 4, 0x00, 0x30, 0x50)
    schedule.add(3, 5, 4, 0x00, 0x30, 0x50)
    schedule.add(3, 9, 4, 0x00, 0x30, 0x50)
    schedule.add(3, 13, 4, 0x00, 0x30, 0x50)

    schedule.add(4, 1, 4, 0x00, 0x30, 0x50)
    schedule.add(4, 5, 4, 0x00, 0x30, 0x50)
    schedule.add(4, 9, 4, 0x00, 0x30, 0x50)
    schedule.add(4, 13, 4, 0x00, 0x30, 0x50)

    schedule.addEvent(track)
    addMetaEvent(track, 0, smf.MetaEndOfTrack, []byte{})

    // Add track to new midi struct
    err = midi.AddTrack(track)
    checkErr(err)

    // Save to new midi source file
    outputMidi, err := os.Create("outputMidi.mid")
    checkErr(err)
    defer outputMidi.Close()

    // Create buffering stream
    writer := bufio.NewWriter(outputMidi)
    smfio.Write(writer, midi)
    writer.Flush()
}

type schedule struct {
    events [][]event
}

type event struct {
    channel uint8
    note uint8
    velocity uint8
}

func (s *schedule) add(bar, beat, length int, channel, note, velocity uint8) {

    if s.events == nil {
        s.events = make([][]event, 65) // 64個 + 次の先頭1個
    }

    // open
    key := (bar - 1) * 16 + (beat - 1)
    s.events[key] = append(s.events[key], event{channel: channel, note: note, velocity: velocity})
    //log.Println(key, bar, beat, channel, note, velocity)

    // close
    key = (bar - 1) * 16 + (beat - 1) + length
    s.events[key] = append(s.events[key], event{channel: channel, note: note, velocity: 0x00})
    //log.Println(key, bar, beat, channel, note, 0x00)
}

func (s *schedule) addEvent(track *smf.Track) {
    before := 0
    for i, events := range s.events {
        for _, e := range events {
            delta := uint32((i - before) * ticks / 4) // 16分音符まで許容しているので4分のticksを4で割る
            before = i

            //log.Println(before, i, delta, smf.NoteOnStatus, e.channel, e.note, e.velocity)
            addMidiEvent(track, delta, smf.NoteOnStatus, e.channel, e.note, e.velocity)
        }
    }
}

func addMidiEvent(track *smf.Track,
    deltaTime uint32, status, channel, firstDataByte, secondDataByte uint8) {

    event, err := smf.NewMIDIEvent(deltaTime, status, channel, firstDataByte, secondDataByte)
    checkErr(err)

    err = track.AddEvent(event)
    checkErr(err)
}

func addMetaEvent(track *smf.Track,deltaTime uint32, metaType uint8, data []byte) {

    event, err := smf.NewMetaEvent(deltaTime, metaType, data)
    checkErr(err)

    err = track.AddEvent(event)
    checkErr(err)
}

func checkErr(err error) {
    if err != nil {
        log.Fatalln(err)
    }
}
