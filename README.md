# _WAAW04_ midi-generator

毎週1つWebアプリを作ろうの4回目。

期間は18/06/01 - 18/06/07


ゲーム作成で困るであろう素材の一つ、BGMを簡単に作りたい！


## ゴール

* [x] 再生できるmidiファイルを生成できる
* [x] 音楽として成立させられる
* [ ] いくつかのパラメータ(シード値)でテンポや音色を簡単に変更できる
* [ ] 生成したmidiファイルをgolangのweb serverからダウンロードできる
* [ ] 生成したmidiファイルをgolangのweb serverにアクセスしたブラウザから再生できる


## 目的

* [x] MIDIや音楽の理論についての基礎知識
* [ ] シード値を元にしたプロシージャルな仕組み


## 課題

* [x] MIDI生成ライブラリ

    * [algoGuy/EasyMIDI: EasyMidi is a simple and reliable library for working with standard midi file (SMF)](https://github.com/algoGuy/EasyMIDI)

* [x] MIDIの基礎知識

    * [MIDIを学ぼう](http://www1.plala.or.jp/yuuto/midi/index.html)
    * time division: midiファイルの解像度と言えなくもない項目で、時間の細かさを設定する
    * SMF: simple midi formatの略。midiファイルの形式。フォーマット0がシングル、フォーマット1がマルチトラック。
    * track: トラック。CDのトラックなどとは違い、トラックは並列で処理されていく。同時並行で流れるものって認識でよさそう。
    * channel: チャンネル。イメージはテレビのチャンネル？つまり違う音色とかを入れて分離するための単位。
    * status: midiイベントにのみある信号の識別情報。0x90のNoteOnStatusは鍵盤が押されたことを意味するステータス。ステータス毎に必要とするデータの数が違い、1つか2つである。NoteOnStatusの場合は1つ目がノート番号、2つ目がベロシティを表す。
    * type: metaイベントにのみある信号の識別情報。midiイベントのstatusに該当する。説明文や著作権表示からはじまる、トラック終端を意味するメタ情報などもあり重要。テンポ設定もあるけど、トラック毎に違うテンポでもいいの…？
    * [tempo: metaイベントで設定できる4分音符の長さの設定。60bpmなら1秒に1拍なので、1,000,000マイクロ秒。120bpmなら1秒に2拍なので500,000マイクロ秒。3バイトで表す必要がある。](https://sites.google.com/site/yyagisite/material/smfspec#tempo)

* [x] 音楽理論(和音、)

    * [コード（和音）の解説一覧。3和音・セブンス・ディミニッシュ・テンション・sus4・オーグメント | musicwater](http://musicwater.site/music-theory-chord/#i-3)


## 振り返り

0. Idea:

    アイデアやテーマについて
    * BGM作りを自動化、もしくは量産できるのは良い
    * midiやけどDTMの基礎の基礎も少しかじれるのも良い
    * プロシージャルを目標とするのも良い


0. What went right:

    成功したこと・できたこと
    * midiの生成はライブラリのおかげで容易
    * 相対時間を頭から追い出せる設計に出来た
    * tempoはbpmから算出


0. What went wrong:

    失敗したこと・できなかったこと
    * 1週間でmidiの基礎を掴むのは無理だった
    * プロシージャルどころか、リズムの自動化も無理だった
    * 音色、ドラムはさらなる知識が必要


0. What I learned:

    学べたこと
    * midiの基礎。もう少し時間があれば完成させられると思う
    * commitしたらちゃんとpushしよう……

## サンプル

なし
